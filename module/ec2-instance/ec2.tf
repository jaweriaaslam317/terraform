# creating ec2 instance
resource "aws_instance" "myapp-1" {
    ami = data.aws_ami.image-name.id
    instance_type = t2.micro
    availability_zone = var.availability_zone
    key_name = var.terraform_key

    tags = {
    Name = "${var.environment}-ec2"
    Environment = "${var.environment}"
  }

  provisioner "local_exec" {
    working_dir = "/c/Users/sanau/Downloads/cloud documents/Ansible"
    command = "ansible-playbook --inventory ${self.public_ip}, --private-key = ${var.private_key} --user ec2-user deploy-nodejs.yaml"
  }
  provisioner "local_exec" {
    working_dir = "/c/Users/sanau/Downloads/cloud documents/Ansible"
    command = "ansible-playbook --inventory ${self.public_ip}, --private-key = ${var.private_key} --user ec2-user docker.yaml"
  }
  
}
