data "aws_ami" "image-name" {
  most_recent      = true
  owners           = ["self"]

  filter {
    name   = "aws_ami"
    values = ["myami-*"]
  }

}