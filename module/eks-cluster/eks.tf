module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.13.1"

cluster_name = "myapp-eks-cluster"
cluster_version = "1.26"
subnet_ids = module.vpc.private_subnets
vpc_id = module.vpc.vpc_id

tags = {
    environment = "deployment"
}

eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.micro"]
    }
  }

}