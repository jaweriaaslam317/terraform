data "aws_availability_zone" "myapp_az" {
 region = var.region
}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "4.0.1"
  
  name = "myapp-vpc"
  cidr = var.vpc_cidr_block
  private_subnet_names = var.private_subnet_cidr_block
  public_subnet_names = var.public_subnet_cidr_block
  azs = data.aws_availability_zone.myapp_az.names

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags = {
     "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/roles/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/roles/internel-elb" = 1
  }


}