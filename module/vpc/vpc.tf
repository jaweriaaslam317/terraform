resource "aws_vpc" "vpc_1" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "${var.environment}-vpc"
    Environment = "${var.environment}"
  }
}

 #create IGW for the subnet

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc_1.id

  tags = {
    Name = "${var.environment}-IGW"
    Environment = "${var.environment}"
  }
}

# create Elastic ip
resource "aws_eip" "ip" {
  vpc = true
}

# creating NAT

resource "aws_nat_gateway" "nt_gateway" {
  allocation_id = aws_eip.ip.id
  subnet_id = aws_subnet.subnet_1.id

  tags = {
    Name = "${var.environment}-nat-gateway"
    Environment = "${var.environment}"
  }


}
  

# creating Public Subnets

resource "aws_subnet" "subnet_1" {
  vpc_id = aws_vpc.vpc_1.id
  count = length(var.subnet_cidr_block)
  cidr_block = element(var.subnet_cidr_block, count.index)
  availability_zone = element(var.availability_zone, count.index)

  tags = {
    Name = "${var.environment}-${element(var.availability_zone, count.index)}-public-subnet"
    Environment = "${var.environment}"
  }
}

# creating private Subnets

resource "aws_subnet" "private_subnet_1" {
  vpc_id = aws_vpc.vpc_1.id
  count = length(var.private_subnet_cidr_block)
  cidr_block = element(var.private_subnet_cidr_block, count.index)
  availability_zone = element(var.availability_zone, count.index)

  tags = {
    Name = "${var.environment}-${element(var.availability_zone, count.index)}-private-subnet"
    Environment = "${var.environment}"
  }
}


# Route tables to route traffic to private subnet

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc_1.id

  tags = {
    Name = "${var.environment}-route-table"
    Environment = "${var.environment}"
  }
}



# Route tables to route traffic to public subnet

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc_1.id

 
  tags = {
    Name = "${var.environment}-route-table"
    Environment = "${var.environment}"
  }
}

resource "aws_route" "internet" {
  gateway_id = aws_internet_gateway.internet_gateway.id
  route_table_id = aws_route_table.public_route_table.id
  destination_cidr_block = "0.0.0.0/0"

}

# Route table Associateion for public and private subnets

resource "aws_route_table_association" "public_subnet_asso" {
  count = length(var.subnet_cidr_block)
  subnet_id = element(aws_subnet.subnet_1.*.id, count.index)
  route_table_id = aws_route_table.public_route_table.id
}


resource "aws_route_table_association" "private_subnet_asso" {
  count = length(var.private_subnet_cidr_block)
  subnet_id = element(aws_subnet.private_subnet_1.*.id, count.index)
  route_table_id = aws_route_table.private_route_table.id
}




