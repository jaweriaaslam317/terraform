
# create security group

resource "aws_security_group" "sg_1" {
  vpc_id = aws_vpc.vpc_1.id

  ingress{
    from_port        = var.from_ports
    to_port          = var.to_ports
    protocol         = "tcp"
    cidr_blocks      = var.cidr_block
  }

  egress {

    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Environment = "${var.environment}"
  }
}

