output "vpc_id" {
  value = aws_vpc.vpc_1.id
}

output "public_subnet_id" {
  value = aws_subnet.subnet_1.id
}

output "sg_id" {
  value = aws_security_group.sg_1.id
}

output "alb" {
  value = aws_lb.alb.dns_name
}