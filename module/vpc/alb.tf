# create target group
resource "aws_lb_target_group" "test" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_1.id
}

# Attach the target group to the AWS instance
resource "aws_lb_target_group_attachment" "alb_attachment" {
  target_id = aws_instance.ec2_server.id
  target_group_arn = aws_lb_target_group.test.id
  port = 80
}

# create load balancer
resource "aws_lb" "alb" {
  name               = "test-lb-tf"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_1.id]
  subnets            = [for subnet in aws_subnet.public : subnet.id]

  enable_deletion_protection = true

  tags = {
    Name = "${var.environment}-lb"
    Environment = "${var.environment}"
  }
}


# create a listener

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test.arn
  }
}