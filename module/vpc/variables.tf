variable "vpc_cidr_block" {
  type = list
}

variable "environment" {}
variable "subnet_cidr_block" {}
variable "availability_zone" {}
variable "private_subnet_cidr_block" {}
variable "from_ports" {}
variable "to_ports" {}

