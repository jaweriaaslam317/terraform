terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.1.0"
    }
  }

  backend "s3" {
    bucket         	   = "kptest-ansible"
    key              	 = "files/terraform.tfstate"
    region             = "us-east-1"
  }
}

